SITE INDEXER (tt.bench.project)
========================================

1) Download
--------------------------------

### Clone the git Repository from the main repository or fork it to your github account:

Note that you **must** have git installed and be able to execute the `git`
command.

	$ git clone https://xplk@bitbucket.org/xplk/zend_site_indexer.git

2) Installation
---------------

### a) Install the Vendor Libraries

    $ php composer.phar install

### b) Change DBAL settings, create DB and update it
