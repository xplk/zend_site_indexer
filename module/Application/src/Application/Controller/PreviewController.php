<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Model\IndexerDAO;
use Application\Model\IndexerModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\Response;
use Zend\View\Model\ViewModel;

class PreviewController extends AbstractActionController{
    public function indexAction(){
        return new ViewModel();
    }
    public function savedCopyAction(){
        $hash = $this->params()->fromQuery('url');
        $dao = new IndexerDAO();
        $page = $dao->getPageByHashUrl($hash);

        $response='<h1 style="text-align: center">This page have status: ';
        switch($page->getStatus()){
            case IndexerModel::STATUS_AWAITING;
                $response.=' in index query</h1>';
                break;
            case IndexerModel::STATUS_INDEXING:
                $response.=' indexing in process</h1>';
                break;
            case IndexerModel::STATUS_READY:
                $response =
                    '<div style="background:#fff;border:1px solid #999;margin:-1px -1px 0;padding:0;">
                    <div style="background:#eee;border:1px solid #fefefe;color:#000;font:13px arial,sans-serif;font-weight:normal;margin:3px;padding:5px;text-align:left">
                    This is saved version by tt-indexer service. Original content may be changed (Last indexed:'.$page->getDate().')</div>
                    </div>'.
                   '<div style="position:relative">'.$page->getHTML().'</div>';
                break;
            default:
                $response.=' not in database</h1>';
        }
        return $this->getResponse()->setContent($response);
    }
}
