<?php
/**
 * Created by JetBrains PhpStorm.
 * User: istrelnikov
 * Date: 9/19/13
 * Time: 4:43 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Application\Model;

use Zend\Dom\Query;
class Crawler {
    public static function getPage($link){
        require_once('module/Application/src/Application/Misc/url_to_absolute.php');
        $page = array();
        if(Crawler::retrieve_remote_file_size($link)>1024*1024){ // big files
            return null;
        }
        $page['html'] = Crawler::get_url_data($link);
        if(!$page['html']){
            return null;
        }

        /* Get the MIME type and character set */
        preg_match( '@<meta\s+http-equiv="Content-Type"\s+content="([\w/]+)(;\s+charset=([^\s"]+))?@i',
            $page['html'], $matches );
        if (isset($matches[3]))
            $initialEncoding = $matches[3];

        if(isset($initialEncoding)){
            if( $initialEncoding != 'UTF-8' ){
                $page['html'] = preg_replace('/<meta(.+)charset(.+)>/i','<meta http-equiv="content-type" content="text/html; charset=utf-8">', $page['html']);
                $page['html'] = mb_convert_encoding($page['html'],'UTF-8',$initialEncoding);
                }
        }else{
            //TODO: good search for html5 charset ->curl charset -> default 'latin1'
            $charset = mb_detect_encoding($page['html']);
            if($charset!='UTF-8'){
                $page['html'] = mb_convert_encoding($page['html'],'UTF-8',$charset);
            }
            $page['html'] = '<meta http-equiv="content-type" content="text/html; charset=utf-8">' . $page['html'];
        }
       /*** a new dom object ***/
        $dom = new Query($page['html']);
        /*** replace $link if <base> ***/
        $base_tag = $dom->queryXpath('//base');
        $cnt = count($base_tag);
        for($i = 0; $i< $cnt;$i++){
            $link = $base_tag[$i]->getAttribute['href'];
        }
        /*** plaintext to index ***/
        $page['plain'] = '';
        $text_tags = $dom->queryXpath("//text()[not(ancestor::script)][not(ancestor::style)][not(ancestor::noscript)][not(ancestor::form)]");

        $cnt = count($text_tags);
        for($i = 0; $i< $cnt;$i++){
            $val = trim($text_tags[$i]->nodeValue);
           if(strlen($val) > 3){
                $page['plain'] .= $val . ' ';
            }
        };

        /*** title ***/
        $title_tag = $dom->queryXpath('//title');
        $cnt = count($title_tag);
        for($i = 0; $i< $cnt;$i++){
            $page['title'] = $title_tag[$i]->childNodes->item(0)->nodeValue;
        }
        /*** save inner JS ***/
        preg_match_all("/<script[^>]*>(.*)<.*script>/Uis",
            $page['html'], $innerJS );
        $page['html'] = preg_replace("/<script[^>]*>(.*)<.*script>/Uis",'!!!PUT_SCRIPT_HERE!!!',$page['html']);
        $dom = new Query($page['html']);
        $a_tags = $dom->queryXpath('//a');
        /*** links ***/
        $links = array();
        $cnt = count($a_tags);
        for($i = 0; $i< $cnt;$i++){
            $url = $a_tags[$i]->getAttribute('href');
            $url = url_to_absolute($link,$url);
            $links[] = $url;
            $a_tags[$i]->setAttribute('href',$url);
        }

        $page['html'] = $a_tags->getDocument()->saveHTML();
        $dom = new Query($page['html']);

        $frame_tags = $dom->queryXpath('//iframe');
        $cnt = count($frame_tags);
        for($i = 0; $i< $cnt;$i++){
            $url = $frame_tags[$i]->getAttribute('src');
            $url = url_to_absolute($link,$url);
            $links[] = $url;
            $frame_tags[$i]->setAttribute('src',$url);
        }
        $page['html'] = $a_tags->getDocument()->saveHTML();

        $page['links'] = $links;

        foreach($innerJS[0] as $js){
            $url = Crawler::matchProperty($js,"<script","</script>","src");
            $url = url_to_absolute($link,$url);
            $scripts[] = $url;
            $js = Crawler::replaceProperty($js,"<script","</script>","src",$url,1);
            $page['html']= preg_replace("/!!!PUT_SCRIPT_HERE!!!/",$js,$page['html'],1);
        }
        return $page;
    }

    private static function retrieve_remote_file_size($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_NOBODY, TRUE);
        curl_exec($ch);
        $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        curl_close($ch);
        return $size;
    }

    private static function get_url_data($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36');
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    public static function replaceProperty($data, $start, $end, $property, $alias, $limit = -1){
        //get blocks formed as: $start $property = "..." $end or $start $property = '...' $end
        $pattern = "!(".$start."){1}([^>]*?)".$property."\s*=\s*[\"\'](.*?)[\"\'](.*?)(".$end."){1}!s";
        $data = preg_replace($pattern, "{$start}\${2}{$property}=\"{$alias}\"\${4}{$end}", $data, $limit);
        return $data;
    }

    public static function matchProperty($data, $start, $end, $property){
        //get blocks formed as: $start $property = "..." $end or $start $property = '...' $end
        $pattern = "!(".$start."){1}([^>]*?)".$property."\s*=\s*[\"\'](.*?)[\"\'](.*?)(".$end."){1}!s";
        preg_match($pattern, $data, $data);
        return $data[3];
    }
}